package Task1.domain;

public class Password {
    private String passwordStr;

    public Password(String Pas) {

        this.passwordStr = Pas;
    }

    public String checkPassword() {
        if (this.passwordStr.length() < 9) {
            return "false";
        } else {
            int A = 0;
            int B = 0;
            int C = 0;

            for (int i = 0; i < passwordStr.length(); i++) {
                if (passwordStr.charAt(i) == ' ') {
                    return "false";
                }

                if (this.isDigit(passwordStr.charAt(i))) {
                    C++;
                }

                if (this.hasLowerCase(passwordStr.charAt(i))) {
                    B++;
                }

                if (this.hasUpperCase(passwordStr.charAt(i))) {
                    A++;
                }
            }

            if (A > 0 && B > 0 && C > 0) {
                return passwordStr;
            } else {
                return "false";
            }
        }
    }

    private boolean isDigit(char ch) {
        for (char i = '0'; i <= '9'; ++i) {
            if (ch == i) {
                return true;
            }
        }

        return false;
    }

    private boolean hasUpperCase(char ch) {
        for (char i = 'A'; i <= 'Z'; ++i) {
            if (ch == i) {
                return true;
            }
        }

        return false;
    }

    private boolean hasLowerCase(char ch) {
        for (char i = 'a'; i <= 'z'; ++i) {
            if (ch == i) {
                return true;
            }
        }

        return false;
    }


    @Override
    public String toString() {
        return this.passwordStr;
    }
}




