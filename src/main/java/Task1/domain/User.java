package Task1.domain;

public class User {
    private int id;
    private static int id_gen = 1;
    private static int max = 1;
    private String name;
    private String surname;
    private String username;
    private String password;

    public User(String name, String surname, String P) {
        this.id = id_gen++;
    }

    public User(String name, String surname){
        this.setName(name);
        this.setSurname(surname);
    }

    public User(String name, String surname, String username, String password) {
        this(name, surname);
        this.setUsername(username);
        this.setPassword(password);
    }

    public User(int id, String name, String surname, String username, String password) {
        this.id = id;
        if (max < id) {
            max = id;
        }

        id_gen = max;
        id_gen++;
        this.setName(name);
        this.setSurname(surname);
        this.setUsername(username);
        this.setPassword(password);
    }

    public void setName(String name) {

        this.name = name;
    }

    public void setSurname(String surname) {

        this.surname = surname;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {

        this.password = password;
    }

    public String getName() {

        return this.name;
    }

    public String getSurname() {

        return this.surname;
    }

    public String getUsername() {

        return this.username;
    }

    public String getPassword() {
        return this.password;
    }

    public int getId() {

        return this.id;
    }

    @Override
    public String toString() {
        return this.id + " " + this.name + " " + this.surname + " " + this.username + " " + this.password;
    }
}

